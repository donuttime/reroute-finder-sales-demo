import React, { Component } from 'react';

export default class StartLocation extends Component {

    constructor(props) {
        super(props);

        this.locationChange = this.locationChange.bind(this);
        this.getLocation = this.getLocation.bind(this);

        this.state = {
            "address" : "40.74702, -73.99097",
            "address2" : "40.74702, -73.99097",
        };
    }

    locationChange(e) {
        this.setState({"address": e.target.value})
    }
    
    locationChange2(e) {
        this.setState({"address2": e.target.value})
    }

    getLocation() {
        return this.state.address;
    }

    getLocation2() {
        return this.state.address2
    }

    render () {
        return <div>
            <input style={{width: "30%", marginRight: "6%"}}type="text" value={this.state.address} onChange={this.locationChange}/>
            <input style={{width: "30%", marginLeft: "6%"}}type="text" value={this.state.address2} onChange={this.locationChange2}/>
        </div>
    }
}
